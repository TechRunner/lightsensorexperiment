#!/usr/bin/env python3
import socket
import time

def genSocket():
    return socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def setServer(s, port):
    s.bind((socket.gethostname(), port))
    s.listen(5)

filename = "lightdata.csv"
port = 9411

s = genSocket()
setServer(s, port)

conn, add = s.accept()

f = open(filename, "w")

while True:
    data = conn.recv(1024)
    data = data.decode("utf-8")
    if data:
        t = time.localtime()
        print("Recieved: " + data)
        f.write(f"{t.tm_hour}:{t.tm_min}:{t.tm_sec}, {t.tm_mday}:{t.tm_mon}:{t.tm_year}, " + data + "\n");
        f.flush()
    if not data:
        break

s.shutdown(socket.SHUT_RDWR)
s.detach()
f.close()