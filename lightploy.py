#!/usr/bin/env python3
import matplotlib.pyplot as plt
import csv

with open('lightdata.csv') as csvfile:
    csvdata = csv.reader(csvfile, delimiter=",")
    data = []
    for row in csvdata:
        data.append(row[2])

    for i in range(len(data)):
        data[i] = int(data[i])

    plt.plot(data)
    plt.xlabel("Seconds")
    plt.ylabel("Light Level")
    plt.show()
