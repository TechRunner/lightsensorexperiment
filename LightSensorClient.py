#!/usr/bin/env python3
import network
import machine
import socket
import time

wlan = network.WLAN()
wlan.connect("CruNet", "")

if wlan.isconnected():
    print("Is connected")

adc = machine.ADC(0)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.connect(("10.47.20.56", 9411))

while True:
    data = bytes(str(adc.read()), "utf-8")
    s.send(data)
    time.sleep(5)
